import {sum} from './sum';
import {sub} from './sub';
import {multi} from './multi'

export function concatCOM(a:string, b:string):string{
    return a + ' com ' + b;
}

export function concatSEM(a:string, b:string):string{
    return a + ' sem ' + b;
}

export function concat(a:string, b:string):string{
    return a + ' ' + b;
}

console.log(concat('Aiai', 'DISCORD'));
console.log(sum(2, 1));
console.log(sub(6, 5));
console.log(multi(2, 10));
