- Iniciando projeto nodejs: $ npm init -y
- Instalando typescript: $ npm install -D typescript
- Instalando jest: $ npm install -D jest
- Instalando pacote js-test: $ npm install -D ts-jest 
	- Suporte a source map e type checking ao rodar testes
- Instalando pacotes de tipos do jest: $ npm install -D @types/jest

- Criando js.config.js: $ npx ts-jest config:init
- Criando tsconfig.json: $ npx tsc --init
