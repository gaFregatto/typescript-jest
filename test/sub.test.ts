import {sub} from '../src/sub';

describe('testing sub file', () => {
    test('sub function', () => {
        expect(sub(2, 5)).toBe(-3);
    });
});