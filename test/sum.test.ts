import {sum} from "../src/sum";

describe('testing sum file', () => {
    test('sum function', () => {
        expect(sum(3, 7)).toBe(10);
    });
});