import {multi} from "../src/multi";

describe('testing multi file', () => {
    test('multi function', () => {
        expect(multi(3, 7)).toBe(21);
    });
});