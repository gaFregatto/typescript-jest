import {concatCOM, concatSEM, concat} from "../src/index";

describe("testing index file", () => {
    test("testing concatcom function", () => {
        expect(concatCOM("abacaxi", "pera")).toBe("abacaxi com pera")
    });

    test("testing concatsem function", () => {
        expect(concatSEM("abacaxi", "pera")).toBe("abacaxi sem pera")
    });

    test("testing concat", () => {
        expect(concat("cafe", "frio")).toBe("cafe frio")
    });
});